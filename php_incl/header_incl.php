<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: header_incl.php
 */

if (isset($_POST['resendVerification']) && $_POST['resendVerification'] != NULL){
    // Grabbing AuthCode from DB
    $sql = "SELECT authCode, memberID FROM member WHERE mail = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('s', $_POST['email']);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($authCode, $user_id);
    /* execute prepared statement */
    if (!$stmt->execute()) {
        echo "database execute error address<br />";
        echo("Statement failed: ". $mysqli->error . "<br />". $mysqli->errno ."");
        exit();
    }


    $receiver = $_POST['email'];
    $subject = "Konifehr.ch: Konto Verifizierung";
    $message = "Bitte klicken Sie auf folgenden Link um Ihr Konto zu verifizieren: <br />
							<a href=http://192.168.153.130/konifehr.ch/index.php?page=regcomplete&id=$user_id&code=$authCode>
							http://192.168.153.130/konifehr.ch/index.php?page=regcomplete&id=$user_id&code=$authCode</a>";

    $header  = "MIME-Version: 1.0\r\n";
    $header .= "From: do-not-reply@konifehr.ch'\r\n";

    //define mail text
    $header .= "Content-Type: text/html\n";
    $header .= "Content-Transfer-Encoding: 8bit\n\n";

    mail($receiver, $subject, $message, $header);
}


?>

	<div id="main_nav" class="navbar navbar-default" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
			<button class="navbar-toggle" data-target="#konifehr-navbar-collapse" data-toggle="collapse" type="button">
				<span class="sr-only">Toggle navigation</span> 
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.php?page=home">
			 <div id="navbar_brand_area"></div>
			 </a>
			 </div>
				<div id="konifehr-navbar-collapse" class="navbar-collapse collapse">
					<ul class="nav navbar-nav nav-konifehr navbar-right">
						<?php
                        //todo: modal wird auf dem Smartphone nicht richtig angezeigt
                        //todo: if user isn't activated, resend mail option
                        if (isset($_SESSION["cart"])) {
                            echo "<li ><a href='index.php?page=cart'>
                                <div class='glyphicon glyphicon glyphicon-briefcase btn-costum'></div>
                                <div class='nav-font-size'>Korb</div>
                            </a></li>";
                        }
                        if (isset($_SESSION["email"])){
										echo "<li>
                                                <a class='dropdown-toggle nav-font-size' data-toggle='dropdown' href='#' role='button' aria-expanded='false'>
                                                <div class='glyphicon glyphicon-log-out btn-costum'></div>
                                                <div class='nav-font-size'>Account<span class='caret caret-correction'></span></div></a>
                                                <ul class='dropdown-menu level2-dropdown' role='menu'>

											";
                                    //special admin menues
                                    if(isset($_SESSION['admin'])){
                                        echo "<li><a href='index.php?page=showUser'>
												    <div > Benutzer </div>
											    </a></li>
											     <li><a href='index.php?page=showProducts'>
												    <div >Produkte</div>
											    </a></li>
											";
                                    }
                                //end ul lvl 2 and li
                                echo "<li><a href='index.php?page=address'>
                                            <div >Adresse</div>
                                        </a></li>
                                         <li><a href='index.php?". $_SERVER['QUERY_STRING'] ."&action=logout'>
                                            <div >Logout</div>
                                        </a></li>
                                    </ul></li>";

								}else{ 
									echo "<li data-toggle='modal' data-target='#login_Window'><a href='#'>
										    <div class='glyphicon glyphicon-log-in btn-costum'></div>
										    <div class='nav-font-size'>Login</div>
									</a></li>";
						}
						?>
					</ul> <!-- end ul navbar pulled right -->
					
					<!-- Start menu login -->
					<div class="modal fade" id="login_Window" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button class="close" aria-hidden="true" data-dismiss="modal" type="button">X</button>
									<h1>Login</h1>
								</div>  <!-- close modal header -->
								<div id="main_login" class="modal-body">

                                    <?php if(isset($_SESSION["loginerror"])){
                                        echo "<div class='login_error'>". $_SESSION['loginerror'] ."</div>";
                                    }; ?>
									<form method="post">
										<ul>
											<li><input type="email" placeholder="E-Mail" name="email" size="29" value="<?php echo $_POST['email']; ?>"></li>
											<li><input type="password" placeholder="Passwort" name="passwd" size="29"></li>
											<li><input type="submit" class="btn-primary" value="Anmelden" name="signIn">
                                                <a class="loginModalHint" href="index.php?page=reset">Passwort vergessen?</a>
                                            </li>
										</ul>
									</form>
								</div> <!-- close modal body -->
								<div class="modal-footer">
									<p>Noch kein Login?</p>
									<a class="loginModalHint" href="index.php?page=reg">Jetzt registrieren</a>
								</div>
							</div> <!-- cloase modal content -->
						</div> <!-- close modal dialog -->
					</div> <!-- close modal -->
					<div class="navbar-center">
						<ul class="nav navbar-nav nav-konifehr">
							<li> <a href="index.php?page=home">
								<div class="glyphicon glyphicon-home btn-costum"></div>
								<div class="nav-font-size">Home</div>
							</a></li>
							<li> <a href="index.php?page=news">
								<div class="glyphicon glyphicon-envelope btn-costum"></div>
								<div class="nav-font-size">News</div>
							</a></li>
                            <li><!-- li all about koni -->
                                <a class='dropdown-toggle nav-font-size' data-toggle='dropdown' href='#' role='button' aria-expanded='false'>
                                    <div class='glyphicon glyphicon-asterisk btn-costum'></div>
                                    <div class='nav-font-size'>Über<br /><span class='caret caret-correction'></span></div></a>
                                <ul class='dropdown-menu level2-dropdown' role='menu'>
                                    <li style='text-align: left;'><a href='index.php?page=about#author_about'>
                                            <div > KoniFehr </div></a>
                                    </li>
                                    <li style='text-align: left;'><a href='index.php?page=about#author_publication'>
                                            <div > Publikationen </div></a>
                                    </li>
                                    <li style='text-align: left;'><a href='index.php?page=about#author_pictures'>
                                            <div >Fotos</div></a>
                                    </li>
                                    <li style='text-align: left;'  ><a href='index.php?page=about#author_contact'>
                                            <div >Kontakt</div></a>
                                    </li>
                                </ul>
                            </li>
							<li><a href="index.php?page=store">
								<div class="glyphicon glyphicon-shopping-cart btn-costum"></div>
								<div class="nav-font-size">Store</div>
							</a></li>
						</ul>	
				</div> <!-- end navbar-center -->
			</div>	<!-- end navbar-collapse -->
		</div> <!-- end container-fluid -->
	</div> <!-- end navbar fixed top -->

    <!-- force modal to start, when loginerror is set -->
    <?php if($_SESSION['loginerror']): ?>
        <script> $('#login_Window').modal('show');</script>
        <?     unset($_SESSION['loginerror']);
        endif;?>

