<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: head_incl.php
 */

	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);

	include_once 'php/db_connect.php';
	$loginerror="";
	/* User Login */
	session_start();
	// stops JS being able to access the session id.
	$httponly = true;
    $loginerror ="";
	
	//logout process 
	if($_GET["action"]=='logout' && isset($_SESSION["email"])){
		//new session id
		session_regenerate_id(true);
		session_destroy();
        header('location: index.php?page=logout');
	}
	elseif(isset($_POST["email"]))
	{
	/* Check Session */
	if ($user = $mysqli->prepare("SELECT memberID, password, salt, admin, authenticated
        FROM member
       WHERE mail = ?
        LIMIT 1")) {
        	/* Bind email to $user */
        	$user->bind_param('s', $_POST["email"]); 
			/* execute the prepared query */
			$user->execute();
			$user->store_result();
			
			/* get variable from result*/
			$user->bind_result($user_id, $db_password, $salt, $admin, $authenticated);
       	    $user->fetch();
			
			/* hash pw with salt*/
			$password = hash('sha256', $_POST["passwd"] . $salt);
			/* check, if the user exist */
			if ($user->num_rows == 1) {

				if ($password == $db_password){
                    if ($admin == 1) {
                        $_SESSION["admin"] = TRUE;
                    }
                    if($authenticated == 1) {
                        $_SESSION["email"] = $_POST["email"];
                        header('location: index.php?page=home');
                    }else{
                        $mailVerification = $_POST['email'];
                        $loginerror = "Account nicht aktiviert.
                                       <form method='post'>
                                         <input type='submit' name='resendVerification' value='Mail erneut senden?'>
                                          <input type='hidden' name='actMail' value='$mailVerification'>
                                       <form>";
                        $_SESSION["loginerror"] = $loginerror;
                    }
				}
                else if(isset($_POST['resendVerification'])){
                    $loginerror = " Das Bestätigungsmail wurde erneut versendet";
                    $_SESSION["loginerror"] = $loginerror;
                } else {
                    // Password is wrong
                    // record this attempt in the database
                    $now = time();
                    $mysqli->query("INSERT INTO login_attempts(memberID, time)
                                    VALUES ('$user_id', now())");

                     $loginerror = " E-Mail oder Passwort ist falsch";
                     $_SESSION["loginerror"] = $loginerror;
				}

			} else{
                    $loginerror = " E-Mail oder Passwort ist falsch";
                    $_SESSION["loginerror"] = $loginerror;
                }
    }
		
}	
?>

<!DOCTYPE html>
<!-- check if js is enabled. Class will be replaced via JS -->
<html lang="en" class="no-js">
<head>
		<meta charset="utf-8">
		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<title>KoniFehr.ch</title>
		<meta name="description" content="Koni is a well known blogger.">
		<meta name="author" content="Gabriel Wehrli">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width; initial-scale=1">

		<!-- Fav and Apple IOS homescreen icon -->
		<link rel="shortcut icon" href="favicon.ico">
		<!-- not created till now -->
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
		
		<!-- CSS inc -->
		<!-- <link rel="stylesheet" href="css/reset.css"> -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/konifehr.css">
	
		<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    
		<!-- jQuery einbinden -->
		<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>

		<!-- JS einbinden -->
		<script type="text/javascript" src="js/konifehr.js"></script>
		<script type="text/javascript" src="js/chkForm.js"></script>
</head>

