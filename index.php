<?php
include_once "php_incl/head_incl.php";
include_once "php_incl/header_incl.php";
include_once("php_incl/analyticstracking.php");
?>
    <div id="main_area container-fluid">
        <?php
        $availableSites = array("about", "news", "contact", "pictures", "publication", "store", "home", "showUser", "reg", "product", "cart",
            "checkout", "regcomplete", "logout", "reset", "success", "error", "address", "showProducts", "editProduct");
        if (in_array($_GET['page'], $availableSites)){
            include_once 'php'.DIRECTORY_SEPARATOR.$_GET['page'].'.php';
        }
        elseif(!isset($_GET['page'])){
            include_once 'php'.DIRECTORY_SEPARATOR.'home.php';
        }
        else {
            echo "page doesn't exist";
            exit;
        }
        ?>
    </div>
<?php
include "php_incl/footer_incl.php";
?>