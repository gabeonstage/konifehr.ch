<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: about.php
 */

//todo: header Anpassung: download file http://fcontheweb.com/articles/force_download/
//todo: autho publication: link2product

$stmt = $mysqli->prepare("SELECT address.givenname, address.middlename, address.surname, author.pseudonym, author.description, author.imageSource
			FROM author
            INNER JOIN address ON author.authorID = address.addressID
			WHERE author.authorID = 1");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($addressGivenname, $addressMiddlename, $addressSurname, $authorPseudonym, $authorDescription, $authorImageSource);
$stmt->fetch();


echo "<body>";

echo " <div class='row'>";
echo "<div id='author_page' class='col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1'>"; //div overall

//start content

//initialize about top navbar
echo "<div id='about_nav' class='side-nav'>";
echo "<ul id='about_nav_ul' class='nav nav-tabs' role='tablist'>";
echo "<li>";
echo "<a href='#author_about'>Über </a>";
echo "</li>";
echo "<li>";
echo "<a href='#author_publication'>Publikationen </a>";
echo "</li>";
echo "<li>";
echo "<a href='#author_contact'>Kontakt </a>";
echo "</li>";
echo "<li>";
echo "<a href='#author_pictures'>Bilder </a>";
echo "</li>";
echo "</ul>";
echo "</div>"; //end div navigation

//author about start
echo "<div id='author_about' class='col-xs-12'>";
echo "<h3>".$addressGivenname." ".$addressMiddlename." ".$addressSurname."</h3>";
echo "</div>";
echo "<div class='col-xs-12'>";
echo "<img alt='$authorImageSource' src='media/images/author/medium/$authorImageSource'>";
echo "</div>"; //end div author_pictures
echo "<div id='author_description' class='col-xs-12'>";
echo utf8_encode($authorDescription);
echo "</div>"; //end div author_description
//author about end

//author contact start - scrollspy
echo "<div id='author_contact' class='box col-xs-12'>";
echo "<h3>Kontakt</h3>";

echo "<address style='margin-top: 20px;'>";
echo  "<strong>Peter Michael Wehrli</strong><br>";
echo "Im Hummel 12<br>";
echo "8716 Schmerikon<br>";
echo "<a href='mailto:#'>peter.michael.wehrli@gmail.com</a>";
echo "</address>";
echo "</div>";
//end div author contact



//author publication start - scrollspy
echo "<div id='author_publication' class='box col-xs-12'>";
echo "<h3>Publikationen</h3>";

$stmt = $mysqli->prepare("SELECT book.name, product.productID, price, imageSource, `isbn-13`
                              FROM product
                              LEFT JOIN book ON product.productID = book.productID
                              LEFT JOIN columns ON product.productID = columns.productID");
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($bookname, $productID, $price, $imageSource, $isbn13);

    //initialize table
    echo "<div class='table-responsive'>";
    echo "<table class='table'>";
    echo "<thead>";
    echo "<th> Bild </th>";
    echo "<th> Name </th>";
    echo "<th> ISBN </th>";
    echo "</thead>";
    echo "<tbody>";


	while ($row = $stmt->fetch()) {
        if ($productID <99) {
            echo "<tr>
                    <td><img alt='Bild' src='media/images/store/small/$imageSource'> </td>
                    <td>$bookname</td>
                    <td>$isbn13</td>
                </tr>";
        }
    }
echo "<tr>
        <td><img alt='Bild' src='media/images/store/small/glarner_alpbuch.jpg'> </td>
        <td><a class='normal_link' href='http://www.glarneralp.ch/t3/' target='_blank'>GlarnerAlpbuch (Mitautor)</td>
        <td>9783033046771</a></td>
    </tr>";
echo  "</tbody>";
echo  "</table>";
echo "</div>";
echo "</div>";
//end div author publication


//author pictures start - scrollspy
echo "<div id='author_pictures' class='box col-xs-12'>";
echo "<h3>Bilder</h3>";
echo "<div class='author_pictures_box col-xs-4'>";
echo    "<div class='col-xs-12 no_padding img_center'><img alt='Peter Wehrli' style='margin-top: 5px;' src='media/images/author/medium/Peter-Michael-Wehrli.jpg'> </div>";
echo    "<div class='col-xs-12'>";
echo        "<div class='col-xs-12' style='text-align: center; font-size: 20px;'> Download: </div>";
echo        "<div class='col-xs-6' style='border: solid 0.2px;'><a class='normal_link' href='media/images/author/medium/Peter-Michael-Wehrli.jpg' target='_blank'> mittel </a></div>";
echo        "<div class='col-xs-6' style='border: solid 0.2px;'> <a class='normal_link' href='media/images/author/large/Peter-Michael-Wehrli.jpg' target='_blank'> gross </a> </div>";
echo    "</div>";
echo "</div>";

echo "<div class='author_pictures_box col-xs-4'>";
echo    "<div class='col-xs-12 no_padding img_center'><img alt='Peter Wehrli Bild 1' style='margin-top: 5px;' src='media/images/author/medium/Peter-Michael-Wehrli-1.jpg'> </div>";
echo    "<div class='col-xs-12'>";
echo        "<div class='col-xs-12' style='text-align: center; font-size: 20px;'> Download: </div>";
echo        "<div class='col-xs-6' style='border: solid 0.2px;'><a class='normal_link' href='media/images/author/medium/Peter-Michael-Wehrli-1.jpg' target='_blank'> mittel </a></div>";
echo        "<div class='col-xs-6' style='border: solid 0.2px;'> <a class='normal_link' href='media/images/author/large/Peter-Michael-Wehrli-1.jpg' target='_blank'> gross </a> </div>";
echo    "</div>";
echo "</div>";

echo "<div class='author_pictures_box col-xs-4'>";
echo    "<div class='col-xs-12 no_padding img_center'><img alt='Peter Wehrli Bild 3' style='margin-top: 5px;' src='media/images/author/medium/Peter-Michael-Wehrli-3.jpg'> </div>";
echo    "<div class='col-xs-12'>";
echo        "<div class='col-xs-12' style='text-align: center; font-size: 20px;'> Download: </div>";
echo        "<div class='col-xs-6' style='border: solid 0.2px;'><a class='normal_link' href='media/images/author/medium/Peter-Michael-Wehrli-3.jpg' target='_blank'> mittel </a></div>";
echo        "<div class='col-xs-6' style='border: solid 0.2px;'> <a class='normal_link' href='media/images/author/large/Peter-Michael-Wehrli-3.jpg' target='_blank'  > gross </a> </div>";
echo    "</div>";
echo "</div>";





echo "</div>";
// end author pictures



echo "</div>"; // end div overall
echo "</div>"; //end div row




?>