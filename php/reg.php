<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: reg.php
 */

//initialize variable
$salutationError = $givennameError = $surnnameError = $birthdateError =
$telError = $mailError = $mailreError = $passwordError = $passwdreError =
$plzError = $streetError = $genderError = $mailError = $placeError = "";
//error Variable initialisieren
$errorCount=0;

if ($_SERVER["REQUEST_METHOD"] == "POST"){
		if (empty($_POST["salutation"])) {
			$salutationError="Bitte eine Anrede wählen";
			$errorCount++;
		}
		if (empty($_POST["givenname"])) {
			$givennameError="Bitte einen Vornamen eingeben";
			$errorCount++;
		}
		if (empty($_POST["surname"])) {
			$surnameError="Bitte einen Nachnamen eingeben";
			$errorCount++;
		}
		if (empty($_POST["birthdate"])) {
			$birthdateError="Bitte Geburtstag eingeben";
			$errorCount++;
        }
        if (empty($_POST["tel"])) {
			$telError="Bitte eine Telefonnummer eingeben";
			$errorCount++;
        }
       if (empty($_POST["mail"])) {
			$mailError="E-Mail Adresse fehlt";
			$errorCount++;
		}
		if (!empty($_POST["mail"]) && filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL)) {
				//E-Mail Address is true
			} elseif (!empty($_POST["mail"])) {
				$mailError = "E-Mail falsch";
				$errorCount++;
		}
		if (empty($_POST["mailre"])) {
			$mailreError="E-Mail wiederholen";
			$errorCount++;
		}
		if ($_POST["mailre"] != $_POST["mail"]) {
			$mailreError = "E-Mail entspricht nicht überein";
			$errorCount++;
		}
        if (empty($_POST["password"])) {
            $passwordErrorError="Passwort fehlt";
            $errorCount++;
        }
        if (empty($_POST["passwdre"])) {
            $passwdreError="Bitte Passwort wiederholen";
            $errorCount++;
        }
        if ($_POST["password"] != $_POST["passwdre"]) {
            $passwdreErrorError = "Passwörter stimmen nicht überein";
            $errorCount++;
        }
		if (empty($_POST["street"])) { //proofed
			$streetError=" Bitte eine Strasse eingeben";
			$errorCount++;
		}
		if (empty($_POST["plz"])) { //proofed
			$plzError="PLZ fehlt";
			$errorCount++;
		} else {
			if (preg_match('/[0-9]{4}$/', $_POST["plz"])) {
			} else {
				$plzError="PLZ ungültig";
				$errorCount++;
			}
		}
		if (empty($_POST["place"])) {
			$placeError="Bitte einen Ort angeben";
			$errorCount++;
		}
	}
		if($errorCount==0 && $_SERVER["REQUEST_METHOD"] == "POST") {

			// check if email already exist

			$getEmail = $mysqli->prepare("SELECT * FROM member WHERE mail = ?") or die ("E-Mail kann nicht geprüft werden");
            $getEmail->bind_param('s', $_POST["mail"]);
            $getEmail->execute();
            $getEmail->store_result();
            $countRows = $getEmail->num_rows;

            if ($countRows == 1){

            	$mailError = "E-Mail existiert bereits";

            } else {
            	//create sha256 pw hash
				$password = hash('sha256', $_POST["password"] . $salt);

				//generating an random code for the mail authentification
			    $authCode = substr( md5(rand()), 0, 30);

				///////////// MEMBER INSERT /////////////
				$sql = "INSERT INTO `member` (`mail`, `password`, `authenticated`, `authCode`) VALUES (?, ?, ?, ?)";

				//prepare insert stmt for creating a new user address
				if(!$stmt = $mysqli->prepare($sql)){
					echo "database prepare error member <br />";
                    exit();
				}

				//user not jet authentificated, will be changed after E-Mail authenticated
				$a = 0;
				$stmt->bind_param('ssis', $_POST["mail"], $password, $a, $authCode);

				/* execute prepared statement */
				if (!$stmt->execute()) {
					echo "database execute error member <br />";
                    exit();
				}

				// save id for linked stmt
				$user_id = $stmt->insert_id;


				/////////// MEMBER INSERT END /////////////

				///////////// ADDRESS INSERT /////////////
				$sql = "INSERT INTO `address` (`addressID`, `salutation`, `title`, `givenname`,
								`surname`, `birthdate`, `tel`, `street`, `plz`, `place`)
								VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				//prepare insert stmt for creating a new user address
				if(!$stmt = $mysqli->prepare($sql)){
					echo "database prepare error address<br />";
                    exit();
				}

				$stmt->bind_param('issssissss', $user_id, $_POST["salutation"], $_POST["title"], $_POST["givenname"],
				$_POST["surname"], $_POST["birthdate"], $_POST["tel"], $_POST["street"], $_POST["plz"], $_POST["place"]);

				/* execute prepared statement */
				if (!$stmt->execute()) {
					echo "database execute error address<br />";
					echo("Statement failed: ". $mysqli->error . "<br />". $mysqli->errno ."");
                    exit();
				}
				/////////// ADDRESS INSERT END /////////////


				/* close statement and connection */
				$stmt->close();

				echo "
                    <div class='row'>
                        <div class='positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2'>
                            <div class='infoMessageBody alert alert-pwreset'>
                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
                                <span class='glyphicon glyphicon-star'></span> <strong>Registrierung abgeschlossen</strong>
                                <hr class='message-inner-separator'>
                                <div class=''>
                                    <img class='img-circle' src='media/icons/message_image.gif'>
                                </div>
                                <hr class='message-inner-separator'>
                                <p class='messageFooter'>Danke für die Registrierung. Ein Bestätigungsmail wurde Ihnen an: ". $_POST['mail'] ." gesendet. Bitte die E-Mail bestätigen und danach erneut anmelden</p>
                            </div>
                        </div>
                    </div>
				";

				// todo: sending verification mail, with link included
				$receiver = $_POST['mail'];
				$subject = "Konifehr.ch: Konto Verifizierung";
				$message = "Bitte klicken Sie auf folgenden Link um Ihr Konto zu verifizieren: <br />
							<a href=http://www.konifehr.ch/index.php?page=regcomplete&id=$user_id&code=$authCode>
							http://www.konifehr.ch/index.php?page=regcomplete&id=$user_id&code=$authCode</a>";

                $header  = "MIME-Version: 1.0\r\n";
                $header .= "From: do-not-reply@konifehr.ch'\r\n";

                //define mail text
                $header .= "Content-Type: text/html\n";
                $header .= "Content-Transfer-Encoding: 8bit\n\n";

                mail($receiver, $subject, $message, $header);

				exit();
            }
        }
?>
<h3 class="regHeader"> Konto erstellen </h3>
<div class="row">
	<div class="colRegPic col-lg-6 col-md-7 hidden-sm hidden-xs">
		<img class="img-responsive" src="media/images/pageRegPic.png" alt="" />
	</div>
	<div class="regFormArea col-lg-5 col-md-5 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-offset-1 col-xs-10">
		<form id="formBuy" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>?page=reg" method="POST">
			<div class="formRegister table-responsive">
				<table class="formReg table">
					<tr>
						<td class="regFormHeader">
							Anrede(*)
						</td>
						<td class="regFormHeader">
							Titel
                        </td>
					</tr>
					<tr>
						<td>
							<select name="salutation">
								<option value="">Anrede</option>
								<option value="Frau">Frau</option>
								<option value="Herr">Herr</option>
							</select>
						</td>
						<td>
							<select name="title">
								<option value="">Titel</option>
								<option value="Dr.">Dr.</option>
								<option value="Dr. med.">Dr. med.</option>
								<option value="Dipl.">Dipl.</option>
                                <option value="Dipl.-Ing.">Dipl.-Ing.</option>
                                <option value="Dipl.-Kfr.">Dipl.-Kfr.</option>
                                <option value="Dipl.-Kfm.">Dipl.-Kfm.</option>
								<option value="Prof.">Prof.</option>
								<option value="Prof. Dr.">Prof. Dr.</option>
							</select>
						</td>
					</tr
						<?php
							/*  printout formcheck */
							if (!empty($salutationError)){
								echo "<tr><td class='errorHint'> <span>". $salutationError ."</span></td></tr>";
							}
						?>
					<tr>
						<td class="regFormHeader">
							Vorname(*)
						</td>
						<td class="regFormHeader">
							Name(*)
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" size="25" name="givenname" placeholder="Vorname" value="<?php echo $_POST['givenname']; ?>" onblur="chkInput(this)" />
						</td>
						<td>
							<input type="text" size="25" name="surname" placeholder="Nachname" value="<?php echo $_POST['surname']; ?>" onblur="chkInput(this)"/>
						</td>
					</tr>
							<?php
								/*  printout formcheck */
								if (!empty($givennameError) || !empty($surnameError)){
									echo "<tr><td class='errorHint'> <span>". $givennameError ."</td><td class='errorHint'>". $surnameError ."</span></td></tr>";
								}
							?>
					<tr>
						<td class="regFormHeader">
							Geburtstag(*)
						</td>
						<td class="regFormHeader">
							Telefon(*)
						</td>
					</tr>
					<tr>
						<td><input type="text" size="25" name="birthdate" placeholder="Geburtstag" value="<?php echo $_POST['birthdate']; ?>" onblur="chkInput(this)" /> </td>
						<td><input type="text" size="25" name="tel" placeholder="Telefon/Handy" value="<?php echo $_POST['tel']; ?>" onblur="chkInput(this)"/></td>
					</tr>
							<?php
								/*  printout formcheck */
								if (!empty($birthdateError) || !empty($telError)){
									echo "<tr><td class='errorHint'> <span>". $birthdateError ."</td><td class='errorHint'>". $telError ."</span></td></tr>";
								}
							?>
					<tr>
						<td class="regFormHeader">
							Passwort(*)
						</td>
						<td class="regFormHeader">
							Passwort wiederholen(*)
						</td>
					</tr>
					<tr>
						<td><input type="password" size="25" name="password" placeholder="Passwort" value="<?php echo $_POST['password']; ?>" onblur="chkInput(this)" /></td>
						<td><input type="password" size="25" name="passwdre" placeholder="Passwort wiederholen" value="<?php echo $_POST['passwdre']; ?>" onblur="chkInput(this)" /></td>
					</tr>
					<tr>
							<?php
								/*  printout formcheck */
								if (!empty($passwdError) || !empty($passwdreError)){
									echo "<tr><td class='errorHint'>". $passwdError ."</td><td class='errorHint'>". $passwdreError ."</td></tr>";
								}
							?>
					<tr>
						<td class="regFormHeader">
							E-Mail(*)
						</td>
						<td class="regFormHeader">
							E-Mail wiederholen(*)
						</td>
					</tr>
					<tr>
						<td ><input type="text" size="25" name="mail" placeholder="E-Mail" value="<?php echo $_POST['mail']; ?>" onblur="chkInput(this)" /></td>
						<td><input type="text" size="25" name="mailre" placeholder="E-Mail wiederholen" onblur="chkInput(this)" /></td>
					</tr>
							<?php
								/*  printout formcheck */
								if (!empty($mailError) || !empty($mailreError)){
									echo "<tr><td class='errorHint'>". $mailError ."</td><td class='errorHint'>". $mailreError ."</td></tr>";
								}
							?>
					<tr>
						<td class="regFormHeader">
							Strasse & Nr.(*)
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="text" size="57" name="street" placeholder="Strasse" value="<?php echo $_POST['street']; ?>" onblur="chkInput(this)" />
						</td>
					</tr>
						<?php
							/*  printout formcheck */
							if (!empty($streetError)){
								echo "<tr><td class='errorHint'> <span>". $streetError ."</span></td></tr>";
							}
						?>
					<tr>
						<td class="regFormHeader">
							PLZ(*) Ort(*)
						</td>
					</tr>
					<tr>
						<td colspan="2"><input type="text" size="4" name="plz" placeholder="PLZ" value="<?php echo $_POST['plz']; ?>" onblur="chkInput(this)" />
						<input type="text" size="47" name="place" placeholder="Ort" value="<?php echo $_POST['place']; ?>" onblur="chkInput(this)" /></td>
					</tr>
							<?php
								if (!empty($plzError)){
									echo "<tr><td class='errorHint'>". $plzError ."</td><td class='errorHint'>". $placeError ."</td></tr>";
								 }
							?>
					<tr>
						<td>
							<input type="submit" class="btn btn-primary submit formButton" name="submit" value="Absenden" style="color: black;" />
							<a class="btn btn-primary formButton" type="button" href="index.php?page=reg">
							<span class="glyphicon glyphicon-repeat"> </span> Reset
							</a>
						</td>
					</tr>
				</table>
			</div>  <!-- end div table responsive -->
		</form>
	</div>
</div>
