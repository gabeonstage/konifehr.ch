<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: showProducts.php
 */

if(!isset($_SESSION['admin'])){
    echo "page doesn't exist";
} elseif(isset($_SESSION['admin'])){

    $stmt = $mysqli->prepare("SELECT product.productID, book.name, columns.Name
                              FROM product
                              LEFT JOIN book ON product.productID = book.productID
                              LEFT JOIN columns ON product.productID = columns.productID");
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($productID, $bookName, $columnsName);

    //table header
    echo "
        <div class='row col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2'>
        <div class='table-responsive '>
        <form method='post' action='index.php?page=editProduct'>
        <table class='table'>
        <tr>
         <th>check</th>
         <th>Name</th>
        </tr>
    ";

    while ($row = $stmt->fetch()) {
        //table content
        if($productID < 99){
        echo "
            <tr>
                <td><input type='radio' name='productID' value='$productID'/></td>
                <td>$bookName</td>
            </tr>
        ";
        }else if ($productID > 99){
        echo "
        <tr>
            <td><input type='radio' name='productID' value='$productID'/></td>
            <td>$columnsName</td>
        </tr>
        ";
        }
    }

    //table footer
    echo"
    <tr>
        <td><input type='submit' class='btn btn-primary' name='updateItem' value='bearbeiten' class='submit'></input></td>
    </tr>
    </table>
    </form>
    </div>
    </div><!-- end div row -->
    ";



}


?>