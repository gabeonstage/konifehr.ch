<?php

/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: cart.php
 */

//todo: add transport fee

echo "<div id='shopping_cart class='row'>
		<div class='col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2'>";
			
			if (isset($_GET['cmd']) && $_GET['cmd']=="emptycart") {
				unset($_SESSION["cart"]); 
			}
			
			//remove a specific item from the cart
			if(isset($_POST['removeID']) && $_POST['removeID'] !=""){
				$removeID = $_POST['removeID'];
				if(count($_SESSION['cart'])<=1){
					unset($_SESSION["cart"]); 
				}
				else {
					unset($_SESSION["cart"]["$removeID"]);
					sort($_SESSION["cart"]);
				}
				
			}
			
			/**** building cart view ****/
			$cartVisual="";
			$cartTotal="";
			if (!isset($_SESSION["cart"]) || count($_SESSION["cart"])<1) {
				/* Output message if the cart is empty */
				$cartVisual = '<div class="row">
                                <div class="positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2">
                                    <div class="infoMessageBody alert alert-pwreset">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <span class="glyphicon glyphicon-star"></span> <strong>Fehler</strong>
                                        <hr class="message-inner-separator">
                                        <div class="">
                                            <img class="img-circle" alt="test" src="media/icons/message_image.gif">
                                        </div>
                                        <hr class="message-inner-separator">
                                        <p class="messageFooter">Dein Einkaufskorb ist leider noch leer :(</p>
                                    </div>
                                </div>
                            </div>';
			}
			else{
				$i=0;
				//head of cart table
				$cartVisual .= "<div class='table-responsive'>";
				$cartVisual .= "<table class='table'>";
				$cartVisual .= "<thead>";
				$cartVisual .= "<tr>";
				$cartVisual .= "<th> Bild </th>";
				$cartVisual .= "<th> Name </th>";
				$cartVisual .= "<th> Author </th>";
				$cartVisual .= "<th> Typ </th>";
				$cartVisual .= "<th> Preis </th>";
				$cartVisual .= "<th> Remove </th>";
				$cartVisual .= "</tr>";
				$cartVisual .= "</thead>";
				
				foreach ($_SESSION["cart"] as $each_item){					
					$productID = $each_item['productID'];
					
					//if the product is a book
					if ($productID < 100) {
						// get additional information to the selected product in the cart from the db
						$stmt = $mysqli->prepare("SELECT book.name, author.pseudonym, product.price, product.imageSource
							FROM book
							LEFT JOIN product ON book.productID = product.productID
							LEFT JOIN author ON book.authorFS = author.authorID
							WHERE book.productID = ?");
						$stmt->bind_param('i', $productID);
						$stmt->execute();
						$stmt->store_result();
						$stmt->bind_result($bookName, $authorPseudonym, $productPrice, $imageSource);
						$stmt->fetch();
						//looping every product price in the cart to the cartTotal Variable
						$cartTotal = $productPrice + $cartTotal;
						//define the region and money format, else there would be a problem with decimal calculations
						setlocale(LC_MONETARY, "de_CH");
						$cartTotal = money_format("%10.2n", $cartTotal);
						//design the shopping cart with all informations
						$cartVisual .= "<tbody>";
						$cartVisual .= "<tr>";
						$cartVisual .= "<td><img alt='Bild' src='media/images/store/small/$imageSource'> </td>";
						$cartVisual .= "<td>" .$bookName. "</td>";
						$cartVisual .= "<td>" .$authorPseudonym. "</td>";
						$cartVisual .= "<td>" .$each_item['bookType']. "</td>";
						$cartVisual .= "<td> CHF " .$productPrice. "</td>";
						$cartVisual .= "<td> <form action='index.php?page=cart' method='post'>
												<input name='deleteBtn" . $productID ."' type='submit' value='X' />
												<input name='removeID' type='hidden' value='" . $i . "' />
											</form>
										</td>";
						$cartVisual .= "</tr>";
							
					}
					//if the product is a fanarticle
					elseif ($productID > 99) {
                        // get additional information to the selected product in the cart from the db
                        $stmt = $mysqli->prepare("SELECT columns.name, product.price, product.imageSource
							FROM columns
							LEFT JOIN product ON columns.productID = product.productID
							WHERE columns.productID = ?");
                        $stmt->bind_param('i', $productID);
                        $stmt->execute();
                        $stmt->store_result();
                        $stmt->bind_result($columnsName, $productPrice, $imageSource);
                        $stmt->fetch();
                        //looping every product price in the cart to the cartTotal Variable
                        $cartTotal = $productPrice + $cartTotal;
                        //define the region and money format, else there would be a problem with decimal calculations
                        setlocale(LC_MONETARY, "de_CH");
                        $cartTotal = money_format("%10.2n", $cartTotal);
                        //design the shopping cart with all informations
                        $cartVisual .= "<tbody>";
                        $cartVisual .= "<tr>";
                        $cartVisual .= "<td><img alt='Bild' src='media/images/store/small/$imageSource'> </td>";
                        $cartVisual .= "<td>" .$columnsName. "</td>";
                        $cartVisual .= "<td></td>";
                        $cartVisual .= "<td>" .$each_item['bookType']. "</td>";
                        $cartVisual .= "<td> CHF " .$productPrice. "</td>";
                        $cartVisual .= "<td> <form action='index.php?page=cart' method='post'>
												<input name='deleteBtn" . $productID ."' type='submit' value='X' />
												<input name='removeID' type='hidden' value='" . $i . "' />
											</form>
										</td>";
                        $cartVisual .= "</tr>";
					}
				$i++;
				} //2nd of foreach loop
				
				// footer of the cart visual
				//buiding the cart total row
				$cartVisual .= "<tr class='cartTotalPrice'>";
				$cartVisual .= "<td></td>";
				$cartVisual .= "<td></td>";
				$cartVisual .= "<td></td>";
				$cartVisual .= "<td>Total:</td>";
				$cartVisual .= "<td>CHF $cartTotal </td>";
				$cartVisual .= "<td> <a class='test' href='index.php?page=cart&cmd=emptycart'>X</a></td>";
				$cartVisual .= "</tr>";
				//building the checkout row
				$cartVisual .= "<tr>";
				$cartVisual .= "<td>
									<form name='checkout' action='index.php?page=checkout' method='post'>
										<button class='btn btn-lg'> Zur Kasse
										</button>
									</form>
								</td>";
				$cartVisual .= "<td></td>";
				$cartVisual .= "<td></td>";
				$cartVisual .= "<td></td>";
				$cartVisual .= "<td></td>";
				$cartVisual .= "<td></td>";
				$cartVisual .= "</tr>";
				$cartVisual .= "</tbody>";
				$cartVisual .= "</table>"; //end table
				$cartVisual .= "</div>"; //end div table responsive
				
			} // end of else

			
		//show the shopping cart
		echo "$cartVisual
	</div>
</div>";
?>