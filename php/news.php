<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: news.php
 */
//todo: integrate this part into the db and make it more dynamical
//todo: //todo: http://bootsnipp.com/snippets/featured/bootstrap-lightbox

//SQL Query

$stmt = $mysqli->prepare("SELECT `date`, title, image, description
                          FROM news ORDER BY `date` DESC LIMIT 5");
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($mysqlDate, $newsTitle, $newsImage, $newsDescription);

//convert to the european format
$newsDate = date("d.m.Y", strtotime($mysqlDate));


// From MySQL to Euro 2 $mysql_date = '2011-12-26'; 3 $euro_date = date('d/m/Y', strtotime($mysql_date));

echo "<div id='author_page' class='col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12'>"; //div overall
echo '<div class="row">';


while ($row = $stmt->fetch()) {
    //convert date to the european format
    $newsDate = date("d.m.Y", strtotime($mysqlDate));
    echo  "<div class='positioningCenter col-md-10 col-md-offset-1 col-sm-offset-1 col-sm-10 col-xs-10 col-xs-offset-1'>
             <div class='infoMessageBody alert alert-news'>
                 <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>
                     ×</button>
                 <span class='glyphicon glyphicon-star'></span> <strong>$newsDate - ".utf8_encode($newsTitle)."</strong>
            <hr class='message-inner-separator'>
            <div class=''>
                <a class='thumbnail' data-toggle='modal' data-target='#lightbox'>
                    <img class='img-responsive img_center' src='media/images/news/$newsImage'>
                </a>
            </div>
            <hr class='message-inner-separator'>
            <p class='messageFooter'> ".utf8_encode($newsDescription)."</p>
            </div>
          </div>";
}
echo '</div>';

echo '<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
        <div class="modal-content">
            <div class="modal-body">
                <img src="" alt="" />
            </div>
        </div>
    </div>';


echo "</div>";

?>