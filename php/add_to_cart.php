<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: add_to_cart.php
 */
//this site wont be opened through index.php
session_start();
if (isset($_POST["productID"])){

        $productID = $_POST["productID"];
        $bookType = $_POST["options"];
        $wasFound = false;

        /*check if already an item in the cart */
        if(!isset($_SESSION["cart"]) || count($_SESSION["cart"]) < 1){
            //cart is empty
            $_SESSION["cart"] = array(0 => array("productID" => $productID, "bookType" => $bookType));
        }else {
        $i=0;
        // cart has items on it
            foreach($_SESSION["cart"] as $each_item){
                $i++;
                    while(list($key, $value) = each($each_item)){
                        if ($key == "productID" && $value == $productID) {
                                /* item is already in the cart, add a new one */
                                array_push($_SESSION["cart"], array("productID" => $productID, "bookType" => $bookType));
                            $wasFound = true;
                        }
                    } //end while
            } //end foreach loop
            if($wasFound == false){
                array_push($_SESSION["cart"], array("productID" => $productID, "bookType" => $bookType));
            }


        } //end else condition
        header("location: ../index.php?page=cart");

    }
?>