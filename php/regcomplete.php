<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: regcomplete.php
 */

//todo: Bestätigungsmail überarbeiten
//todo: generate md5 hash for regmail
//grabbing the get var from the E-Mail link
$id = $_GET['id'];
$authCode = $_GET['code'];

$stmt = $mysqli->prepare("SELECT authCode, authenticated FROM member WHERE memberID = ?") or die ("Account kann nicht geprüft werden");
$stmt->bind_param('i', $id);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($authCodeDB, $authBitDB);
$stmt->fetch();

if($authCode != $authCodeDB){
    echo "dieser Eintrag existiert nicht";
}

elseif($authCode == $authCodeDB) {

    if($authBitDB == "0"){
        //change the authenticated value in the db
        $stmt = $mysqli->prepare("UPDATE `praxispo_konifehr`.`member` SET `authenticated` = '1' WHERE `member`.`memberID` = ?");
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->store_result();
        $stmt->fetch();
        echo '<div class="row">
        <div class="positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2">
            <div class="infoMessageBody alert alert-pwreset">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <span class="glyphicon glyphicon-star"></span> <strong>Registrierung abgeschlossen</strong>
                <hr class="message-inner-separator">
                <div class="">
                 <img class="img-circle" alt="test" src="media/icons/message_image.gif">
               </div>
                <hr class="message-inner-separator">
                <p class="messageFooter"> Ihre Registrierung wurde erfolgreich abgeschlossen. Sie können sich nun über den Login-Bereich anmelden </p>
            </div>
        </div>
    </div>';
    }else{
        echo '<div class="row">
        <div class="positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2">
            <div class="infoMessageBody alert alert-pwreset">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <span class="glyphicon glyphicon-star"></span> <strong>Registrierung bereits abgeschlossen</strong>
                <hr class="message-inner-separator">
                <div class="">
                 <img class="img-circle" alt="test" src="media/icons/message_image.gif">
               </div>
                <hr class="message-inner-separator">
                <p class="messageFooter"> Die Registrierung wurde bereits abgeschlossen</p>
            </div>
        </div>
    </div>';
    }

}

?>