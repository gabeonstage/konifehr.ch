<script>
    $(document).ready(function(){
            var hContent = $("body").height(); // get the height of your content
            var hWindow = $(window).height();  // get the height of the visitor's browser window
            var bottom_right = $('#bottom_right');
            // if the height of your content is bigger than the height of the
            // browser window, we have a scroll bar

            //todo: raffi Div ausblenden beim scrollen: $(this).scrollTop() = 0 &&

            if( hContent>hWindow) {
                bottom_right.addClass("bottom_right_active");
            }else {
                bottom_right.removeClass("bottom_right_active");
            }
    });

</script>

<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: home.php
 */

	$query = "SELECT quote FROM quote ORDER BY RAND()";
	$result = $mysqli->query($query);
while($row = $result->fetch_array())
{
$rows[] = $row;
}

$quote1 = $rows[0][quote];
$quote2 = $rows[1][quote];
$quote3 = $rows[2][quote];
$quote4 = $rows[3][quote];
$quote5 = $rows[4][quote];


//todo: redesign page for mobile <720px with horizontal pictures and smaller font

echo "<div id='bottom_right' class='glyphicon glyphicon-arrow-down icon_bottom_right'>
</div>
<div id='home_image_slider' class='carousel slide' data-ride='carousel'>
	<ol class='carousel-indicators'>
		<li data-target'#home_image_slider' data-slide-to='0' class='active'></li>
		<li data-target'#home_image_slider' data-slide-to='1'></li>
		<li data-target'#home_image_slider' data-slide-to='2'></li>
		<li data-target'#home_image_slider' data-slide-to='3'></li>
		<li data-target'#home_image_slider' data-slide-to='4'></li>
	</ol>

	<div class='carousel-inner'>
		<div class='item active'>
			<img class='image_slider_dimensions' src='media/images/quote/1.jpg' alt='image slider 1'>
			<div class='carousel-caption'>
				<h3><span class='quote_intro'> &rdquo; </span>" .utf8_encode($quote1). "</h3>
				<h3> KoniFehr</h3>
			</div>
		</div> 
		<div class='item'>
			<img class='image_slider_dimensions' src='media/images/quote/2.jpg' alt='image slider 2'>
			<div class='carousel-caption'>
				<h3><span class='quote_intro'> &rdquo; </span>" .utf8_encode($quote2). "</h3>
				<h3> KoniFehr</h3>
			</div>
		</div>
		<div class='item'>
			<img class='image_slider_dimensions' src='media/images/quote/3.jpg' alt='image slider 3'>
			<div class='carousel-caption'>
				<h3><span class='quote_intro'> &rdquo; </span>" .utf8_encode($quote3). "</h3>
				<h3> KoniFehr</h3>
			</div>
		</div>
		<div class='item'>
			<img class='image_slider_dimensions' src='media/images/quote/4.jpg' alt='image slider 4'>
			<div class='carousel-caption'>
				<h3><span class='quote_intro'> &rdquo; </span>" .utf8_encode($quote4). "</h3>
				<h3> KoniFehr</h3>
			</div>
		</div>
		<div class='item'>
			<img class='image_slider_dimensions' src='media/images/quote/5.jpg' alt='image slider 5'>
			<div class='carousel-caption'>
				<h3><span class='quote_intro'> &rdquo; </span>" .utf8_encode($quote5). "</h3>
				<h3> KoniFehr</h3>
			</div>
		</div>
	</div>"
?>

  <a class="left carousel-control" href="#home_image_slider" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#home_image_slider" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div> <!-- End home_image_slider -->