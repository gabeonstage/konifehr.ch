<?php
/**
* Created by PhpStorm.
* User: Gabriel Wehrli
* Title: product.php
*/
?>

<script type="text/javascript">
function chkBuy(input){
var bookTypeCheck = $('input:radio[name=options]:checked').val();
if ( !bookTypeCheck ) {
    alert("Bitte wählen Sie einen Buchtyp");
	return false;
	}
}


</script>
<?php
	if ($_GET['id'] < 100) {
		$stmt = $mysqli->prepare("SELECT product.productID, book.name, book.subtitle,
            book.description, book.language, book.pageCount, product.price, product.imageSource,
            author.pseudonym, author.description, author.imageSource, book.printable, book.ebook, book.pdf, `book`.`isbn-13`, `book`.`isbn-10`, book.year
			FROM book
			LEFT JOIN product ON book.productID = product.productID
			LEFT JOIN author ON book.authorFS = author.authorID
			WHERE book.productID = ?");
		$stmt->bind_param('i', $_GET['id']);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($productID, $bookName, $bookSubtitle, $bookDescription, $bookLanguage, $bookPageCount, $productPrice,
                           $imageSource, $authorPseudonym, $authorDescription, $authorImageSource, $bookprintable, $bookebook, $bookpdf, $bookISBN13, $bookISBN10, $bookYear);
		$stmt->fetch();
		echo " <div class='row'>
				<div class='product_header col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-9 col-xs-offset-3'>
					<h2> $bookName </h2>
					<h4> $bookSubtitle </h4>
						<em data-toggle='modal' data-target='#author_detail_area'>von<a class='normal_link' href='#'>
						   $authorPseudonym
						</a></em>
						<div class='modal fade' id='author_detail_area' tabindex='-1' role='dialog' aria-labelledny='myModalLabel' aria-hidden='true'>
							<div class='modal-dialog'>
								<div class='modal-content'>
									<div class='modal-header'>
										<button class='close' aria-hidden='true' data-dismiss='modal' type='button'>X</button>
										<h3>$authorPseudonym</h3>
									</div> <!-- close modal header -->
									<div class='modal-dialog'>
									    <img alt='$authorImageSource' src='media/images/author/small/$authorImageSource'><BR />
										<span>".utf8_encode($authorDescription)."</span>
									</div> <!-- close modal dialog -->
									<div class='modal-footer'>
										<!-- <p>Weitere Bücher</p> -->
									</div>
								</div> <!-- close modal content -->
							</div>
					
					</div>
				</div>
			  </div                                                                         >
				<div class='row'>
			
				<div class='product_image col-lg-2 col-lg-offset-4 col-md-2 col-md-offset-3 col-sm-3 col-sm-offset-2 col-xs-9 col-xs-offset-3'>
					<img alt='test' src='media/images/store/medium/$imageSource'><br/>
					<h4> Preis: CHF $productPrice</h4>
					<em>
						<span class='glyphicon glyphicon-book'></span>
					 		$bookPageCount Seiten
					</em>
				</div>
				<div class='product_details col-lg-3 col-md-4 col-sm-5 col-sm-offset-0 col-xs-8 col-xs-offset-3'> <!--start main row -->
					<h4 class='product_description_header'> Beschreibung</h4> <span>".utf8_encode($bookDescription)." </span> <br />
					
					<!-- shopping cart try -->
							
					<form method='post' action='php/add_to_cart.php' onSubmit='return chkBuy();'>
						<div class='btn-group' data-toggle='buttons'>";
							  if ($bookprintable == 1) {
							  	echo "<label class='btn btn-koni'>
							  			<input type='radio' name='options' id='option1' value='printout'>Ausdruck</input>
							  		</label>";
							  }
							  if ($bookebook == 1){
							  	echo "<label class='btn btn-koni'>
							  			<input type='radio' name='options' id='option1' value='eBook'>eBook</input>
							  		  </label>";
							  }
							  if ($bookpdf == 1) {
							  	echo "<label class='btn btn-koni'>
							  			<input type='radio' name='options' id='option3' value='pdf'>PDF</input>
							  		  </label>";
							  }
							  echo "</div> <!-- close button group -->
							
							<!-- shopping cart  -->
							<button class='btn btn-md add_shopping_cart col-xs-12'> In den Einkaufskorb
							</button>
				            <input type='hidden' name='productID' value='$productID' />
						</div>
					</form>	 
			    </div>
			</div>
			<div class='book_aditional_infos col-lg-5 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-9 col-xs-offset-3'>
			    <div>
			        <div class='col-lg-10 col-md-10 col-md-10 col-sm-10 col-xs-12'>
                        <h4>Zusätzliche Informationen</h4>
                        <span>Erscheinungsjahr: $bookYear</span><br />
                        <span>Sprache: $bookLanguage</span><br />
                        <span>ISBN 10: $bookISBN10</span><br />
                        <span>ISBN 13: $bookISBN13</span>
			        </div>
			    </div>
			</div> <!-- end div book_aditional_infos -->";
	}
	/* column part for VA is disabled, first. can be later used */
    /*
	elseif ($_GET['id'] > 99) {
		$stmt = $mysqli->prepare("SELECT product.productID, columns.name, columns.description, columns.week, product.price, product.imageSource
			FROM columns
			LEFT JOIN product ON columns.productID = product.productID
			WHERE columns.productID = ?");
		$stmt->bind_param('i', $_GET['id']);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($productID, $columnsName, $columnsDescription, $columnsWeek, $productPrice, $imageSource);
		$stmt->fetch();
		
		echo "<div class='row'>
				    <div class='product_header col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-9 col-xs-offset-3'>
				    	<h2> $columnsName </h2>
                    </div>
              </div>
			  </div>
				<div class='row'>
				<div class='product_image col-lg-2 col-lg-offset-4 col-md-2 col-md-offset-3 col-sm-3 col-sm-offset-2 col-xs-9 col-xs-offset-3'>
					<img alt='$imageSource' src='media/images/store/medium/$imageSource'><br/>
				<h4> Preis: $productPrice</h4>
				</div>
				<div class='product_details col-lg-3 col-md-4 col-sm-5 col-sm-offset-0 col-xs-8 col-xs-offset-3'> <!--start main row -->
					<h4 class='product_description_header'> Beschreibung</h4> ".utf8_encode($columnsDescription)." </span> <br />

					<!-- shopping cart try -->
					<form method='post' action='php/add_to_cart.php'>
					    <div class='btn-group' data-toggle='buttons'>
                             <label class='btn btn-koni'>
                                 <input type='radio' name='options' id='option1' value='pdf'>Digitale Version</input>
                             </label>
                        </div>
							<!-- shopping cart try -->
							<button class='btn btn-md add_shopping_cart col-xs-12'>In den Einkaufskorb
							</button>
				            <input type='hidden' name='productID' value='$productID' />
					</form>
			    </div>
			</div>";
	} */

?>


