<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: db_connect.php
 */

include_once 'db_config.php';
$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}
?>
