<?php
/**
 * Created by PhpStorm.
 * User: gabeonstage
 * Date: 21/11/14
 * Time: 16:38
 */

include_once '../php/db_connect.php';

if (isset($_POST['resendVerification'])){
    echo $_POST['email'];
    // Grabbing AuthCode from DB
    $sql = "SELECT authCode, memberID FROM member WHERE mail = ?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('s', $_POST['email']);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($authCode, $user_id);
    /* execute prepared statement */
    if (!$stmt->execute()) {
        echo "database execute error address<br />";
        echo("Statement failed: ". $mysqli->error . "<br />". $mysqli->errno ."");
        exit();
    }


    $receiver = $_POST['email'];
    $subject = "Konifehr.ch: Konto Verifizierung";
    $message = "Bitte klicken Sie auf folgenden Link um Ihr Konto zu verifizieren: <br />
							<a href=http://192.168.153.130/konifehr.ch/index.php?page=regcomplete&id=$user_id&code=$authCode>
							http://192.168.153.130/konifehr.ch/index.php?page=regcomplete&id=$user_id&code=$authCode</a>";

    $header  = "MIME-Version: 1.0\r\n";
    $header .= "From: do-not-reply@konifehr.ch'\r\n";

    //define mail text
    $header .= "Content-Type: text/html\n";
    $header .= "Content-Transfer-Encoding: 8bit\n\n";

    mail($receiver, $subject, $message, $header);

    exit();
}

?>