<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: address.php
 */

// todo: mail Verification anpassen und verificated Ändern auf 0
//todo: Log erstellen. Benutzer mit allen Meldungen wie E-Mail wurde geändert und beim nächsten mal Anmelden muss folgende Adresse verwendet werden.
// todo: E-Mail change, new message, E-Mail history

if(!isset($_SESSION['email'])){

    echo '<div class="row">
        <div class="positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2">
            <div class="infoMessageBody alert alert-pwreset">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <span class="glyphicon glyphicon-star"></span> <strong>Anmeldung erforderlich</strong>
                <hr class="message-inner-separator">
                <div class="">
                 <img class="img-circle" alt="test" src="media/images/icons/message_image.gif">
               </div>
                <hr class="message-inner-separator">
                <p class="messageFooter"> Um Ihre Daten einsehen zu können, müssen Sie sich zuerst anmelden  </p>
            </div>
        </div>
    </div>';

} elseif(isset($_SESSION['email'])){

    $passwordError = $passwdreError = $errorCount = $mailErrorCount = "";

    //update and check the Password (only if not null
    if(isset($_POST['password']) && $_POST['password'] != NULL) {
        //passwd validation
        if (empty($_POST["password"])) {
            $passwordErrorError = "Passwort fehlt";
            $errorCount++;
        }
        if (empty($_POST["passwdre"])) {
            $passwdreError = "Bitte Passwort wiederholen";
            $errorCount++;
        }
        if ($_POST["password"] != $_POST["passwdre"]) {
            $passwdreError = "Passwörter stimmen nicht überein";
            $errorCount++;
        }
        if ($errorCount == 0) {
            $password = hash('sha256', $_POST["password"] . $salt);
            $stmt = $mysqli->prepare("UPDATE member SET password = ? WHERE mail = ?");
            $stmt->bind_param('ss', $password, $_SESSION['email']);
            $stmt->execute();
            $stmt->close();
        }
    }
    //check and update the E-Mail
    //new todo: check if mail already exist
    if(isset($_POST['mail']) && $_POST['mail'] != NULL && $_POST['mailre'] != NULL) {
        if (!empty($_POST["mail"]) && filter_var($_POST["mail"], FILTER_VALIDATE_EMAIL)) {
            //E-Mail Address is true
        } elseif (!empty($_POST["mail"])) {
            $mailError = "E-Mail falsch";
            $mailErrorCount++;
        }
        if (empty($_POST["mailre"])) { //proofed
            $mailreError="E-Mail wiederholen";
            $mailErrorCount++;
        }
        if ($_POST["mailre"] != $_POST["mail"]) {
            $mailreError = "E-Mail stimmt nicht überein";
            $mailErrorCount++;
        }

        if ($mailErrorCount == 0) {
            $stmt = $mysqli->prepare("UPDATE member SET mail = ? WHERE mail = ?");
            $stmt->bind_param('ss', $_POST['mail'], $_SESSION['email']);
            $stmt->execute();
            $stmt->close();

            $_SESSION['email'] = $_POST['mail'];
        }
    }
        //Update all Items with the POST vars
        if (isset($_POST['updateItem'])) {
            $stmt = $mysqli->prepare("UPDATE address LEFT JOIN member ON address.addressID = member.memberID SET givenname = ?, surname = ?, birthdate = ?, tel = ?, street = ?, plz = ?, place = ? WHERE member.mail = ?");
            $stmt->bind_param('sssssiss', $_POST['givenname'], $_POST['surname'], $_POST['birthdate'], $_POST['tel'], $_POST['street'], $_POST['plz'], $_POST['place'], $_SESSION['email']);
            $stmt->execute();
            $stmt->close();
        }


    //prepare insert stmt for creating a new user address
    $sql = "SELECT address.givenname, address.surname, address.birthdate, address.tel, address.street, address.plz, address.place, member.mail
            FROM member
            INNER JOIN address ON member.memberID = address.addressID
            WHERE member.mail = ?";

    if(!$stmt = $mysqli->prepare($sql)){
        echo "database prepare error address<br />";
        exit();
    }

    $stmt->bind_param('s', $_SESSION['email']);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($givenname, $surname, $birthdate, $tel, $street, $plz, $place, $mail);
    $stmt->fetch();

?>
<h3 class="regHeader"> Kontodaten ändern </h3>
<div class="row">
	<div class="colRegPic col-lg-6 col-md-7 hidden-sm hidden-xs">
		<img class="img-responsive" src="media/images/pageRegPic.png" alt="" />
	</div>
	<div class="regFormArea col-lg-5 col-md-5 col-md-offset-0 col-sm-8 col-sm-offset-2 col-xs-offset-1 col-xs-10">
		<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>?page=address" method="POST">
			<div class="formRegister table-responsive">
				<table class="formReg table">
                    <tr>
                        <td>
                            <input type="text" size="25" name="givenname" value="<?php echo $givenname ?>"  />
                        </td>
                        <td>
                            <input type="text" size="25" name="surname" value="<?php echo $surname ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="regFormHeader">
                            Geburtstag
                        </td>
                        <td class="regFormHeader">
                            Telefon
                        </td>
                    </tr>
                    <tr>
                        <td><input type="text" size="25" name="birthdate" value="<?php echo $birthdate ?>"  /> </td>
                        <td><input type="text" size="25" name="tel" value="<?php echo $tel ?>" /></td>
                    </tr>
                    <tr>
                        <td class="regFormHeader">
                            Passwort
                        </td>
                        <td class="regFormHeader">
                            Passwort wiederholen
                        </td>
                    </tr>
                    <tr>
                        <td><input type="password" size="25" name="password" placeholder="Passwort"  /></td>
                        <td><input type="password" size="25" name="passwdre" placeholder="Passwort wiederholen"  /></td>
                    </tr>
                    <tr>
                        <?php
                        /*  printout formcheck */
                        if (!empty($passwdError) || !empty($passwdreError)){
                            echo "<tr><td class='errorHint'>". $passwdError ."</td><td class='errorHint'>". $passwdreError ."</td></tr>";
                        }
                        ?>
                    <tr>
                    <tr>
                        <td class="regFormHeader">
                            E-Mail
                        </td>
                        <td class="regFormHeader">
                            E-Mail wiederholen
                        </td>
                    </tr>
                    <tr>
                        <td ><input type="text" size="25" name="mail" placeholder="E-Mail" value="<?php echo $mail ?>"  /></td>
                        <td><input type="text" size="25" name="mailre" placeholder="E-Mail wiederholen"  /></td>
                    </tr>
                    </tr>
                    <?php
                    /*  printout formcheck */
                    if (!empty($mailError) || !empty($mailreError)){
                        echo "<tr><td class='errorHint'>". $mailError ."</td><td class='errorHint'>". $mailreError ."</td></tr>";
                    }
                    ?>
                    <tr>
                    <tr>
                        <td class="regFormHeader">
                            Strasse & Nr.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="text" size="57" name="street" value="<?php echo $street ?>"  />
                        </td>
                    </tr>
                    <tr>
                        <td class="regFormHeader">
                            PLZ Ort
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="text" size="4" name="plz" value="<?php echo $plz ?>"  />
                            <input type="text" size="47" name="place" value="<?php echo $place ?>"  /></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" class="btn btn-primary submit formButton" name="updateItem" value="Ändern" />
                            <a class="btn btn-primary formButton" type="button" href="index.php?page=address">
                                <span class="glyphicon glyphicon-repeat"> </span> Widerrufen
                            </a>
                        </td>
                    </tr>
                    </table>
                </div>  <!-- end div table responsive -->
            </form>
        </div>
    </div>

}


<?php


} //end else condition
?>