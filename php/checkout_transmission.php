<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: checkout_transmission.php
 */

//todo: mail to admin after both checkout process
//todo: paypal ipn
//todo: name von pdf attachement beinhaltet den Pfad

session_start();
include_once 'db_connect.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);
//define the cart total var & overView var
$cartTotal = "";
$overviewVisual = "";
$pp_checkout = "";
$i=0;

$product_id_array="";
$pp_checkout ='<form id="paypalSubmit" action="https://www.paypal.com/cgi-bin/webscr" method="post">
         <input type="hidden" name="cmd" value="_cart">
         <input type="hidden" name="upload" value="1">
         <input type="hidden" name="business" value="peter.wehrli@kblachen.ch">';

//head of cart table
$overviewVisual .= "<table>";
$overviewVisual .= "<tr>";
$overviewVisual .= "<th> Name </th>";
$overviewVisual .= "<th> Author </th>";
$overviewVisual .= "<th> Typ </th>";
$overviewVisual .= "<th> Preis </th>";
$overviewVisual .= "</tr>";

//go through the array for the summary
foreach ($_SESSION['cart'] as $each_item){
    $i++;

    //if the product is a book
    if ($each_item['productID'] < 100) {
        //mysql query to gather additional infos about the selected books
        $stmt = $mysqli->prepare("SELECT book.name, author.pseudonym, product.price
                FROM book
                LEFT JOIN product ON book.productID = product.productID
                LEFT JOIN author ON book.authorFS = author.authorID
                WHERE book.productID = ?");
        $stmt->bind_param('i', $each_item['productID']);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($bookName, $authorPseudonym, $productPrice);
        $stmt->fetch();

        //looping every product price in the cart to the cartTotal Variable
        $cartTotal = $productPrice + $cartTotal;

        //define the region and money format, else there would be a problem with decimal calculations
        setlocale(LC_MONETARY, "de_CH");
        $cartTotal = money_format("%10.2n", $cartTotal);

        $x = $i;
        $bookType = $each_item['bookType'];
        $pp_checkout .= '<input type="hidden" name="item_name_' . $x . '" value="' . $bookName . ' (' . $bookType . ')">
                         <input type="hidden" name="amount_' . $x . '" value="' . $productPrice . '" />';

        // Create the product array variable
        $product_id_array .= $each_item['productID'];

        //adding the cart body
        $overviewVisual .= "<tr>";
        $overviewVisual .= "<td>" . $bookName . "</td>";
        $overviewVisual .= "<td>" . $authorPseudonym . "</td>";
        $overviewVisual .= "<td>" . $each_item['bookType'] . "</td>";
        $overviewVisual .= "<td> CHF " . $productPrice . "</td>";
        $overviewVisual .= "</tr>";
    }
    //exclude column preparation
    /*
    elseif ($each_item['productID'] > 99) {
        // get additional information to the selected product in the cart from the db
        $stmt = $mysqli->prepare("SELECT columns.name, product.price, product.imageSource
                                    FROM columns
                                    LEFT JOIN product ON columns.productID = product.productID
                                    WHERE columns.productID = ?");
        $stmt->bind_param('i', $each_item['productID']);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($columnsName, $productPrice, $imageSource);
        $stmt->fetch();
        //looping every product price in the cart to the cartTotal Variable
        $cartTotal = $productPrice + $cartTotal;
        //define the region and money format, else there would be a problem with decimal calculations
        setlocale(LC_MONETARY, "de_CH");
        $cartTotal = money_format("%10.2n", $cartTotal);

        $x = $i;
        $pp_checkout .= '<input type="hidden" name="item_name_' . $x . '" value="' . $columnsName .'">
        <input type="hidden" name="amount_' . $x . '" value="' . $productPrice . '" />';

        //adding the cart body
        $overviewVisual .= "<tr>";
        $overviewVisual .= "<td>" . $columnsName . "</td>";
        $overviewVisual .= "<td></td>";
        $overviewVisual .= "<td></td>";
        $overviewVisual .= "<td> CHF " . $productPrice . "</td>";
        $overviewVisual .= "</tr>";
    } //end elseif */
} //end foreach

// footer of the cart visual
//buiding the cart total row
$overviewVisual .= "<tr>";
$overviewVisual .= "<td></td>";
$overviewVisual .= "<td></td>";
$overviewVisual .= "<td>Total:</td>";
$overviewVisual .= "<td> CHF $cartTotal </td>";
$overviewVisual .= "</tr>";
$overviewVisual .= "</table>"; //end table


if($_POST["submitPayment"] == "paypal"){

    $pp_checkout .= '<input type="hidden" name="custom" value="' . $product_id_array . '">
	        <input type="hidden" name="notify_url" value="http://192.168.153.130/konifehr.ch/php/paypay_ipn.php>
	        <input type="hidden" name="return" value="http://192.168.153.130/konifehr.ch/index.php?page=success>
        	<input type="hidden" name="rm" value="2">
	        <input type="hidden" name="cbt" value="Return to The Store">
	        <input type="hidden" name="cancel_return" value="http://192.168.153.130/konifehr.ch/index.php?page=error">
	        <input type="hidden" name="lc" value="CHF">
	        <input type="hidden" name="currency_code" value="CHF">
	        </form>';

    echo $pp_checkout;
?>
    <!-- JS part for auto Submit Paypal -->
    <p>Sie werden in K&uumlrze zu PayPal weitergeleitet..</p>
   <script type="text/javascript">
        document.getElementById("paypalSubmit").submit();
    </script>

<?php
    echo $pp_checkout;

}elseif($_POST["submitPayment"] == "prepay"){

    $address = $_SESSION["address"];
    // sending verification mail to user, with link included
    $receiver = $_POST['mail'];
    $subject = "Bestellbestätigung";
    $message = '<html>
                   <head>
                   </head>
                   <body> <h1>Konifehr.ch Bestellbestätigung</h1>
                   Herzlichen Dank für die Bestellung von KoniFehr. Die Bestellung wird an folgende Anschrift oder elektronisch geliefert:<br /><br />
                   '.$address.' <br /><br/> Die folgenden Produkte werden geliefert: <br /><br />'. $overviewVisual .'<br /><br />
                   Freundliche Grüsse<br />
                   KoniFehr-Team
                   </body>
                   </html>';

    //prepare attachement
    $file = "../media/attachement/zahlungsinformationen.pdf";
    $type = "application/pdf";

    $attachement = fread(fopen($file, "r"), filesize($file));
    $attachement = chunk_split(base64_encode($attachement));

    //prepare unique attachement, for mail separation
    $boundary = md5(uniqid(time()));


    $header  = "MIME-Version: 1.0\r\n";
    $header .= "From: do-not-reply@konifehr.ch'\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=".$boundary."\n\n";
    $header .= "This is a multi-part message in MIME format -- Dies ist eine mehrteilige Nachricht im MIME-Format.\n";

    //define mail text
    $header .= "--".$boundary."\n";
    $header .= "Content-Type: text/html\n";
    $header .= "Content-Transfer-Encoding: 8bit\n\n";
    $header .= $message."\n";

    //define mail attachement
    $header .= "--".$boundary."\n";
    $header .= "Content-Type: ".$type."; name=\"".$datei."\"\n";
    $header .= "Content-Transfer-Encoding: base64\n";
    $header .= "Content-Disposition: attachment; filename=\"".$file."\"\n\n";
    $header .= $attachement."\n";

    $header .= "--".$boundary."--\n";

    if(@mail($receiver, $subject, $message, $header)){
        //if order is successful send mail to admin
        $receiver = "gabriel.wehrli@gmail.com";
        $subject = "erfolgreiche Bestellung";
        $message = "Der Benutzer". $_SESSION['email'] ." hat soeben folgende Bestellung per Rechnung getätigt<br/>".$overviewVisual." bitte an foldende Adresse liefern:<br />".$address;

        $header  = "MIME-Version: 1.0\r\n";
        $header .= "From: do-not-reply@konifehr.ch'\r\n";

        //define mail text
        $header .= "Content-Type: text/html\n";
        $header .= "Content-Transfer-Encoding: 8bit\n\n";
        //send mail to admin
        mail($receiver, $subject, $message, $header);

        header('location:../index.php?page=success');
    }else{
        //if order is not successful send mail to admin
        $receiver = "gabriel.wehrli@gmail.com";
        $subject = "erfolgreiche Bestellung";
        $message = "Der Benutzer". $_SESSION['email'] ." hat soeben versucht folgende Bestellung per Rechnung zu tätigen. Bestellung ist fehlgeschlagen";

        $header  = "MIME-Version: 1.0\r\n";
        $header .= "From: do-not-reply@konifehr.ch'\r\n";

        //define mail text
        $header .= "Content-Type: text/html\n";
        $header .= "Content-Transfer-Encoding: 8bit\n\n";
        //send mail to admin
        mail($receiver, $subject, $message, $header);

        header('location:../index.php?page=error');
    }
}else{
    header('location:../index.php?page=checkout');
    exit();

}
?>