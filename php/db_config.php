<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: db_config.php
 */

/* DB login details */
define("HOST", "127.0.0.1");
define("USER", "root");
define("PASSWORD", "roller");
define("DATABASE", "konifehr.ch");

define("CAN REGISTER", "any");
define("DEFAULT_ROLE", "member");
?>