<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: logout.php
 */

if(!isset($_SESSION['email'])) {

    ?>

    <div class="row">
        <div
            class="positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2">
            <div class="infoMessageBody alert alert-pwreset">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <span class="glyphicon glyphicon-star"></span> <strong>Ausloggen</strong>
                <hr class="message-inner-separator">
                <div class="">
                    <img class="img-circle" alt="test" src="media/icons/message_image.gif">
                </div>
                <hr class="message-inner-separator">
                <p class="messageFooter">Sie haben sich erfolgreich ausgeloggt. </p>
                <?php unset($_SESSION['cart']); ?>
            </div>
        </div>
    </div>

<?php
} else{
    echo "Bitte über das Menü ausloggen";
}
?>
