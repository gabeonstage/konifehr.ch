<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: editProduct.php
 */

$productID = $_POST['productID'];
//if the product is a book
if($_POST['productID'] < 99 ){

    //Update Books
    if (isset($_POST['updateItem']) && isset($productID)) {
        /* $stmt = $mysqli->prepare("UPDATE product LEFT JOIN book ON product.productID = book.bookID SET book.name = ?, book.subtitle = ?,
             book.year = ?, book.language = ?, `book`.`isbn-10` = ?, `book`.`isbn-13` = ?, book.pageCount = ?, product.price = ?,
            product.imageSource = ?, book.printable = ?, book.ebook = ?, book.pdf = ? WHERE product.productID = ?");
        $stmt->bind_param('ssisssissiiii', $_POST['$bookName'], $_POST['$bookSubtitle'], $_POST['$bookYear'], $_POST['$bookLanguage'], $_POST['$bookISBN10'],
            $_POST['$bookISBN13'], $_POST['$bookPageCount'], $_POST['$productPrice'], $_POST['$imageSource'], $_POST['$bookprintable'], $_POST['$bookebook'], $_POST['$bookpdf'], $productID);
        $stmt->execute();
        $stmt->close(); */
    }

    $stmt = $mysqli->prepare("SELECT book.name, book.subtitle,
            book.description, book.year, book.language, `isbn-10`, `isbn-13`, book.pageCount, product.price, product.imageSource,
             book.printable, book.ebook, book.pdf
			FROM book
			LEFT JOIN product ON book.productID = product.productID
			LEFT JOIN author ON book.authorFS = author.authorID
			WHERE book.productID = ?");
    $stmt->bind_param('i', $productID);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($bookName, $bookSubtitle, $bookDescription, $bookYear, $bookLanguage, $bookISBN10,
                       $bookISBN13, $bookPageCount, $productPrice, $imageSource, $bookprintable, $bookebook, $bookpdf);
    $stmt->fetch();
    echo $mysqli->error;

    echo "
        <!-- table header -->
        <div class='row col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-offset-0 col-sm-12 col-xs-12'>
        <div class='table-responsive '>
        <form method='post'>
        <table class='table'>

        <!-- table body -->
        <th>bookName</th>
            <td><input name='bookName' value='".utf8_encode($bookName)."' size='80'/></td>
        </tr>
        <tr>
            <th>bookSubtitle</th>
            <td><input name='bookSubtitle' value='$bookSubtitle' size='80'/></td>
        </tr>
        <tr>
            <th>bookDescription</th>
            <td><textarea name='bookDescription' cols='80' rows='10'>".utf8_encode($bookDescription)."</textarea></td>
        </tr>
        <tr>
            <th>bookYear</th>
            <td><input name='bookYear' value='$bookYear' size='5'/></td>
        </tr>
        <tr>
            <th>bookLanguage</th>
            <td><input name='bookLanguage' value='$bookLanguage' size='5'/></td>
        </tr>
        <tr>
            <th>bookISBN10</th>
            <td><input name='bookISBN10' value='$bookISBN10' size='10'/></td>
        </tr>
        <tr>
            <th>bookISBN13</th>
            <td><input name='bookISBN13' value='$bookISBN13' size='13'/></td>
        </tr>
        <tr>
            <th>bookPageCount</th>
            <td><input name='bookPageCount' value='$bookPageCount' size='5'/></td>
        </tr>
        <tr>
            <th>productPrice</th>
            <td><input name='productPrice' value='$productPrice' size='10'/></td>
        </tr>
        <tr>
            <th>imageSource</th>
            <td><input name='imageSource' value='$imageSource' size='80'/></td>
        </tr>
        <tr>
            <th>bookprintable</th>
            <td><input name='bookprintable' value='$bookprintable' size='3'/></td>
        </tr>
        <tr>
            <th>bookebook</th>
            <td><input name='bookebook' value='$bookebook' size='3'/></td>
        </tr>
        <tr>
            <th>bookpdf</th>
            <td><input name='bookpdf' value='$bookpdf' size='3' /></td>
        </tr>
    ";


    //table footer
    echo"
    <tr>
        <td>
            <input type='submit' class='btn btn-primary' name='updateItem' value='speichern' class='submit' />
            <input type='hidden' name='productID' value='$productID' />
        </td>
    </tr>
    </table>
    </form>
    </div>
    </div><!-- end div row -->
    ";
}
//if the product is a column
else if($_POST['productID'] >99 ){

}

?>