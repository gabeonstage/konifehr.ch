<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: reset.php
 */


?>
<div class="row">
    <div class="positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2">
        <div class="infoMessageBody alert alert-pwreset">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                ×</button>
            <span class="glyphicon glyphicon-star"></span> <strong>Passwort zurücksetzen</strong>
            <hr class="message-inner-separator">
            <div class="">
                <img class="img-circle" src="media/icons/message_image.gif">
            </div>
            <hr class="message-inner-separator">
            <p class="messageFooter"> iese Seite ist noch in Bearbeitung. Um das Kennwort zurücksetzen zu können, bitte kontaktieren Sie den Webmaster: gabriel.wehrli[at]gmail.com </p>
        </div>
    </div>
</div>