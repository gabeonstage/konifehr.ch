<?php
//todo: Bildvorschau

if(isset($_SESSION["admin"])) {

    echo "<div id='store_items_container'>";

    /**
     * Created by PhpStorm.
     * User: Gabriel Wehrli
     * Title: store.php
     */

    //book category intro
    echo "<div class='row'>
                <div class='col-xs-10 col-xs-offset-1'>
                 <div class='productFrame col-lg-3 col-md-4 col-sm-6 col-xs-12'>
                     <div class='product_category_frame'>
                        <a class='product_category'>
                            <figcaption class='product_details'>
                                <h4>Bücher</h4>
                            </figcaption>
                            <figure>
                                <img alt='$imageSource' src='media/icons/books.png'>
                            </figure>
                        </a>
                    </div>
		        </div>";
    $stmt = $mysqli->prepare("SELECT book.name, columns.name, product.productID, price, imageSource
                              FROM product
                              LEFT JOIN book ON product.productID = book.productID
                              LEFT JOIN columns ON product.productID = columns.productID");
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($bookname, $columnsname, $productID, $price, $imageSource);

    while ($row = $stmt->fetch()) {
        /*<!-- column intro --> */ /*
		if ($productID >99 && $productID <101){ 
			echo "<div class='productFrame col-lg-3 col-md-4 col-sm-6 col-xs-12'>
                <div class='product_category_frame'>
                    <a class='product_category'>
                        <figcaption class='product_deils'>
                            <h4>Kolumne</h4>
                        </figcaption>
                        <figure>
                            <img alt='$imageSource' src='media/icons/column.png'>
                        </figure>
                    </a>
				</div>
			</div>";
		} */
        echo "<div class='productFrame col-lg-3 col-md-4 col-sm-6 col-xs-12'>
				<a href='index.php?page=product&id=$productID'>
					<figure>
						<img alt='$imageSource' src='media/images/store/medium/$imageSource' style='margin-top: 2px;'>
					<figcaption class='product_details'>
						<h4>$bookname $columnsname</h4>
						<em>Preis: CHF $price</em>
					</figcaption>
					</figure>
				</a>
			</div>";
    }
    //product.book array end


    echo "</div>"; // div col ends
    echo "</div>"; // div row ends
    echo "</div>"; // store items container ends

}else{
    echo '<div class="row">
        <div class="positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2">
            <div class="infoMessageBody alert alert-pwreset">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <span class="glyphicon glyphicon-star"></span> <strong>Konifehr Store</strong>
                <hr class="message-inner-separator">
                <div class="">
                    <img class="img-circle" alt="test" src="media/icons/closed_sign.jpg">
                </div>
                <hr class="message-inner-separator">
                <p class="messageFooter">Der Store hat leider noch nicht geöffnet.</p>

            </div>
        </div>
    </div>';
}
?>