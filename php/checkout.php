<?php

/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: checkout.php
 */

if(!isset($_SESSION["email"])){

    echo ' <div class="row">
        <div class="positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2">
            <div class="infoMessageBody alert alert-pwreset">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    ×</button>
                <span class="glyphicon glyphicon-star"></span> <strong>Anmelden </strong>
                <hr class="message-inner-separator">
                <div class="">
                 <img class="img-circle" alt="test" src="media/icons/message_image.gif">
               </div>
                <hr class="message-inner-separator">
                <p class="messageFooter">Sie müssen sich anmelden um den Einkauf abschliessen zu können</p>
            </div>
        </div>
    </div>';

    exit();
}elseif(!isset($_SESSION["cart"])) {
    echo $cartVisual = '<div class="row">
                                <div class="positioningCenter infoMessage col-md-4 col-md-offset-4 col-sm-offset-3 col-sm-6 col-xs-8 col-xs-offset-2">
                                    <div class="infoMessageBody alert alert-pwreset">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <span class="glyphicon glyphicon-star"></span> <strong>Kein Produkt vorhanden</strong>
                                        <hr class="message-inner-separator">
                                        <div class="">
                                            <img class="img-circle" alt="test" src="media/icons/message_image.gif">
                                        </div>
                                        <hr class="message-inner-separator">
                                        <p class="messageFooter">Bitte ein Produkt auswählen</p>
                                    </div>
                                </div>
                            </div>';
    } else{
    $mail = $_SESSION['email'];
    //SQL query
    $sql = "SELECT address.title, address.salutation, address.givenname, address.surname, address.street, address.plz, address.place
            FROM member LEFT JOIN address ON member.memberID = address.addressID WHERE member.mail=?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('s', $mail);
    $stmt->execute();
    $stmt->bind_result($title, $salutation, $givenname, $surname, $street, $plz, $place);
    $stmt->fetch();
    $stmt->close();

    echo "<form action='./php/checkout_transmission.php' method='POST'>
            <h3 class='checkoutHeader'> Willkommen beim auschecken </h3>
            <div class='row'>
               <div class='checkoutArea col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2'>
                    <div class='row'> <!-- row 2nd lvl -->
                        <div class='col-md-12'>
                            <h4>Zahlungsmethode wählen:</h4>"; ?>
                       <!-- //html part -->
                        </div>
                        <ul class="col-md-6">
                            <li class="dropdown paymentSelection ">
                                <a class="dropdown-toggle" data-toggle="dropdown">

                                <!-- //function show choosen value -->
                                <?php
                                    $paymentValue = "<i class='glyphicon glyphicon-asterisk'></i> Bitte wählen";

                                    if ($_GET['pay'] == 'prepay'){
                                        $paymentValue = "<img src='media/icons/prebuy.png'></i> Rechnung";

                                    } elseif ($_GET['pay'] == "paypal"){
                                        $paymentValue = "<img src='media/icons/paypal.png'> PayPal ";
                                    }

                                    //show the value
                                    echo $paymentValue;

                                ?>

                                <b class='caret'></b></a>
                                <ul class="dropdown-menu col-md-12 col-xs-12">
                                    <li><a href="index.php?page=checkout&pay=prepay"><img src='media/icons/prebuy.png'> Rechnung </a></li>
                                    <li><a href="index.php?page=checkout&pay=paypal"><img src='media/icons/paypal.png'> PayPal </a></li>
                                </ul>
                            </li>
                        </ul>

               <?php

                 //for the form submit
                $submitPayment = $_GET['pay'];
                echo "<input type='hidden' name='submitPayment' value='$submitPayment'></input>";



                //save the payment address for further rendering (email)
                $address =  "$title $salutation $givenname $surname <br /> $street <br /> $plz $place <br /> $mail";
                $_SESSION['address'] = $address;
                    echo "
                    </div>
                        <h4>Anschrift / E-Mail für die Lieferung:</h4>
                            <div class='paymentAddress col-md-6'>
                                <address>
                                    <strong >$title $salutation $givenname $surname </strong><br />
                                    $street <br />
                                    $plz $place <br />
                                    $mail <hr />
                                    <span class='stickyNotes'>*1 Lieferanschrift kann unter Account -> Konto geändert werden<br />
                                    *2 E-Mail wird für eine elektronische Lieferung verwendet </span>
                                </address>


                          </div>
                          <div class='paymentOverview col-md-12'>
                            <h4>Bestellübersicht:</h4>
                          </div>
                             <div class='col-md-12'>
                                  <div>";
                                   //define the cart total var & overView var
                                   $cartTotal = "";
                                   $overviewVisual = "";

                                    //head of cart table
				                    $overviewVisual .= "<div class='table-responsive'>";
			                    	$overviewVisual .= "<table class='table'>";
			                       	$overviewVisual .= "<thead>";
			                    	$overviewVisual .= "<tr>";
                                    $overviewVisual .= "<th> Name </th>";
                                    $overviewVisual .= "<th> Author </th>";
			                    	$overviewVisual .= "<th> Typ </th>";
				                    $overviewVisual .= "<th> Preis </th>";
				                    $overviewVisual .= "</tr>";
				                    $overviewVisual .= "</thead>";

                                   //go through the array for the summary
                                   foreach ($_SESSION['cart'] as $each_item){
                                       $productID = $each_item['productID'];

                                       //if the product is a book
                                       if ($productID < 100) {
                                           //mysql query to gather additional infos about the selected books
                                           $stmt = $mysqli->prepare("SELECT book.name, author.pseudonym, product.price
                                                                    FROM book
                                                                    LEFT JOIN product ON book.productID = product.productID
                                                                    LEFT JOIN author ON book.authorFS = author.authorID
                                                                    WHERE book.productID = ?");
                                           $stmt->bind_param('i', $each_item['productID']);
                                           $stmt->execute();
                                           $stmt->store_result();
                                           $stmt->bind_result($bookName, $authorPseudonym, $productPrice);
                                           $stmt->fetch();

                                           //looping every product price in the cart to the cartTotal Variable
                                           $cartTotal = $productPrice + $cartTotal;

                                           //define the region and money format, else there would be a problem with decimal calculations
                                           setlocale(LC_MONETARY, "de_CH");
                                           $cartTotal = money_format("%10.2n", $cartTotal);


                                           //adding the cart body
                                           $overviewVisual .= "<tbody>";
                                           $overviewVisual .= "<tr>";
                                           $overviewVisual .= "<td>" . $bookName . "</td>";
                                           $overviewVisual .= "<td>" . $authorPseudonym . "</td>";
                                           $overviewVisual .= "<td>" . $each_item['bookType'] . "</td>";
                                           $overviewVisual .= "<td> CHF " . $productPrice . "</td>";
                                           $overviewVisual .= "</tr>";
                                       }
                                       //column checkout
                                       //exclude for the moment
                                       /*
                                       elseif ($productID > 99) {
                                           // get additional information to the selected product in the cart from the db
                                           $stmt = $mysqli->prepare("SELECT columns.name, product.price, product.imageSource
                                                                    FROM columns
                                                                    LEFT JOIN product ON columns.productID = product.productID
                                                                    WHERE columns.productID = ?");
                                           $stmt->bind_param('i', $productID);
                                           $stmt->execute();
                                           $stmt->store_result();
                                           $stmt->bind_result($columnsName, $productPrice, $imageSource);
                                           $stmt->fetch();
                                           //looping every product price in the cart to the cartTotal Variable
                                           $cartTotal = $productPrice + $cartTotal;
                                           //define the region and money format, else there would be a problem with decimal calculations
                                           setlocale(LC_MONETARY, "de_CH");
                                           $cartTotal = money_format("%10.2n", $cartTotal);
                                           //design the shopping cart with all informations
                                           //adding the cart body
                                           $overviewVisual .= "<tbody>";
                                           $overviewVisual .= "<tr>";
                                           $overviewVisual .= "<td>" . $columnsName . "</td>";
                                           $overviewVisual .= "<td></td>";
                                           $overviewVisual .= "<td>" . $each_item['bookType'] . "</td>";
                                           $overviewVisual .= "<td> CHF " . $productPrice . "</td>";
                                           $overviewVisual .= "</tr>";
                                       } // end elsif */
                                   }

                                // footer of the cart visual
                                //buiding the cart total row
                                $overviewVisual .= "<tr>";
                                $overviewVisual .= "<td></td>";
                                $overviewVisual .= "<td></td>";
                                $overviewVisual .= "<td>Total:</td>";
                                $overviewVisual .= "<td>CHF $cartTotal </td>";
                                $overviewVisual .= "</tr>";
                                $overviewVisual .= "</tbody>";
                                $overviewVisual .= "</table>"; //end table
                                $overviewVisual .= "</div>"; //end div table responsive

                                //show the cart output
                                echo $overviewVisual ."
                                  </div>
                            </div> <!-- end of payment overview -->

                            <div class='overviewSubmit col-md-12'>

                                <!-- //for the E-Mail approvement -->

                                  <input name='mail' value='$mail' type='hidden'></input></a>

                                 <!-- //submit the form -->
                                <input class='btn btn-primary btn-md' role='button' value='Absenden' type='submit' style='color: black;'/>
                                <a class='btn btn-primary' role='button' href='index.php?page=cart'>Zurück</button></a>
                            </div>
                         </form> <!-- end form checkout -->
                    </div> <!-- end row 2nd level -->
               </div>
            </div>"; //<!-- //end div row -->
}

?>