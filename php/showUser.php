<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Wehrli
 * Title: showUser.php
 */

//todo: when change user to admin, e-mail is vanishing


if(!isset($_SESSION['admin'])){
    echo "page doesn't exist";
} elseif(isset($_SESSION['admin'])) {

    $message = "";
    /* Delete a row item */
    if (isset($_POST['deleteItem'])) {
        /* Post "edit" for id and first remove address table entries */
        $stmt = $mysqli->prepare("DELETE FROM address WHERE addressID = ?");
        $stmt->bind_param('i', $_POST['edit']);
        $stmt->execute();
        /* then delete from member */
        $stmt = $mysqli->prepare("DELETE FROM member WHERE memberID = ?");
        $stmt->bind_param('i', $_POST['edit']);
        $stmt->execute();
        $stmt->close();
        $message = "The user " . $_['mail'] . " is now deleted";
    }


    /* updating an item */
    /* Update without overwritng the passwordstring */
    if (isset($_POST['updateItem'])) {

        if ($_POST['Password'] != NULL) {
            echo "true";
            $password = hash('sha256', $_POST["password"] . $salt);
            $stmt = $mysqli->prepare("UPDATE member SET password = ? WHERE memberID = ?");
            /* Post "edit" for id*/
            $stmt->bind_param('ss', $password, $_POST['edit']);
            $stmt->execute();
            $stmt->close();
            $message = "you changed the password of: " . $_POST['mail'];
        } /* update a stored item */

        if ($_POST['admin'] != NULL) {
            $stmt = $mysqli->prepare("UPDATE member SET admin = ? WHERE memberID = ?");
            /* Post "edit" for id*/
            $stmt->bind_param('si', $_POST['admin'], $_POST['edit']);
            $stmt->execute();
            $stmt->close();
            $message .= "successful promoted to admin";
        }
        if ($_POST['mail'] != NULL) {
            $stmt = $mysqli->prepare("UPDATE member SET mail = ?,  admin = ? WHERE memberID = ?");
            /* Post "edit" for id*/
            $stmt->bind_param('si', $_POST['mail'], $_POST['edit']);
            $stmt->execute();
            $stmt->close();
            $message .= "you changed: " . $_POST['mail'];
        }

    }else if(!isset($message)){
        $message = "nothing changed";
    }


    /* load the result from the db and show it in a table*/
    $result = $mysqli->prepare('SELECT memberID, mail, admin, password FROM member');
    /* send prepared query to db */
    $result->execute();
    $result->store_result();
    $result->bind_result($id, $mail, $admin, $password);

    /* show log message */


    /* header line of the table */

    echo "<div class='row col-xs-10 col-xs-offset-1'>";
    echo "<h1> All User </h1>";
    echo "Log: " . $message;
    echo "</div>"; //end div row
    echo "<div class='row col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2'>";
    echo "<div class = 'table-responsive'>";
    echo "<table class = 'table'>";
    echo "<thead>";
    echo "<form method='post'>";
    echo "<tr>
            <th></th><th> mail </th><th> password </th><th>admin</th>
          </tr>";
    echo "</thead>";
    echo "<tbody>";


    /* define table colors */
    $tableColor1 = "#dcdcdc";
    $tableColor2 = "#cdcdcd";
    $row_count = 0;

//submit query
    while ($row = $result->fetch()) {
        if ($_POST['edit'] == $id && !isset($_POST['newItem']) && !isset($_POST['updateItem'])) {

            /* output if one radio button is active */


            echo "<tr class='activeTable'>
  			        <td><input type='radio' name='su' value='$id' checked='checked'/></td>
  			        <td><input name='mail' placeholder='$mail' /></td>
  			        <td><input name='password' placeholder='*****' /></td>
  			        <td><input name='admin' placeholder='$admin' /></td>
  			        <td><input type='submit' class='saveEntryInTable' value='save' name='updateItem' /></td>
                  </tr>";
        } /* normal output, when no radio button is checked */
        else {

            /* table with different colors */
            $row_color = ($row_count % 2) ? $tableColor1 : $tableColor2;

            echo "<tr style='background-color:" . $row_color . "'>
  			        <td><input type='radio' name='edit' value='$id' /></td>
  			        <td> $mail</td>
  			        <td> *****</td>
  			        <td> $admin</td>
  		          </tr>";
            /* different color for each tr */
            $row_count++;
        }
    }

    /* Submit button "new item" pushed
    if (isset($_POST['new'])) {
        ++$id;
        echo "<tr class='activeTable'>
  			<td><input type='radio' name='edit' value='$id' checked='checked'></input></td>
  			<td>$id</td><td><input name='username'/></td><td><input type='mail' name='mail'/></td><td class='fixLengthTable'><input type='password' name='passwd'/>
  			</td><td><input type='submit' class='saveEntryInTable' value='save' name='newItem'></input></td>
  		 </tr>";
    } */


    /* footer line of the table */
    echo "<tr><td colspan='5'>
  				<input type='submit' class='btn btn-primary' name='submit' value='edit' class='submit'></input>
  				<input type='submit' class='btn btn-primary' name='deleteItem' value='delete' class='submit'></input>
  		</td></tr>";
    echo "</form>";
    echo "</tbody>";
    echo "</table>";
    echo "</div>";
    echo "</div>"; //end div row

}
?>