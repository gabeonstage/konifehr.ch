//JS Navigation
$(document).ready(function(){

//js is on
document.documentElement.className = document.documentElement.className.replace(/\bno-js\b/g, '') + 'js';

    var main_nav = $('#main_nav');
    var f = false;
	$(window).scroll(function () {
		if ($(this).scrollTop() > 40) {
		    main_nav.addClass("navbar-fixed-top");
            f = true;
		}else {
            if(f === true) {f=false;return;}
			main_nav.removeClass("navbar-fixed-top");
		}
	});

    //function news image gallery
    var $lightbox = $('#lightbox');

    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).find('img'),
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };

        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });

    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');

        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
    //function news image gallery end
});

