/* 
 * old new realized in php
 *
 function chkForm (){
 if (document.buy.givenname.value == "") {
    $('input[name="givenname"]').addClass('formError');
  }
  if (document.buy.surname.value == "") {
    $('input[name="surname"]').addClass('formError');
  }
  if (document.buy.plz.value == "") {
    $('input[name="plz"]').addClass('formError');
  }
  if (document.buy.street.value == "") {
    $('input[name="street"]').addClass('formError');
  }
  if (!(document.buy.gender[0].checked || document.buy.gender[1].checked)){
  	$('input[name="gender"]').addClass('formError');
  }
 if (document.buy.mail.value == "") {
    $('input[name="mail"]').addClass('formError');
  }
}
 */

function chkInput(input){
	if (input.value == "") {
    $(input).addClass('formError');
  }
}

$(document).ready(function(){
	$("#mainarea").unbind('click').on('click', '#formBuy input[type=button]', function(){
		var form = $("#formBuy");
		$.ajax({
			url: form.attr('action'),
			type: 'POST',
			data: form.serialize(),
			complete: function(data){
				var n = $(data.responseText).find('form');
				$("#formBuy").replaceWith(n);
			}
		});
	});
});
